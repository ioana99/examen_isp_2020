package interfata;

import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MainClass {
	public static void main(String args[]) {
		JFrame frame = new JFrame("Aplicatie");
		JPanel panel = new JPanel();
		JButton button = new JButton("Numar nou");
		TextArea text = new TextArea(50, 50);
		
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int numarRandom = (int) (Math.random()*100);
				text.setText(text.getText() + "\n" + numarRandom);
			}
		
		});
		
		panel.add(button);
		panel.add(text);
		
		
		frame.add(panel);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
	}
}
